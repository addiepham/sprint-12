"""
Allow user to listen to different music types. Each audio file plays for 1 minute.
When running RespiratoryDetection.py, the respiratory pattern will be recorded while playing each audio file
"""

# Just import stuffs
from playsound import playsound
import winsound


def metal():
    playsound('Mick Gordon - Inferno.mp3')


def RB():
    playsound('Justin Bieber - Come Around Me.mp3')


def Jazz():
    playsound('321Jazz - Piano Brushin.mp3')


def classic():
    playsound('Yann Tiersen - Comptine Dun Autre Ete.mp3')


def Rap():
    playsound('Eminem - Lose Yourself.mp3')



