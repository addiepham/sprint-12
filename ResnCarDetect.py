"""
This module is intended to analyze respiratory + cardiac signals and see how many breaths/heart beats the
user takes in a certain time depending on the type of music they listen. Each audio file is 1 minute.

The outcome of this project is intended to build a table result that tells the user their Respiratory Rate
vs. Music Type (e.g. Within 1 minute, you take 20 breaths while listening to R&B); and
Cardiac Rate vs. Music Type (e.g Your estimated Beats per Minute (BPM) is 80)

The required device is Lithic sensor. The module to run Lithic sensor is lithic.py

Sometimes, the Lithic sensor might not be able to detect number of breaths/beats per minute very accurately. Hence, it
will return an approximate result.

"""

import asyncio
import json
from time import sleep
from lithic import Lithic
from scipy.signal import butter, sosfilt
from MusicChoice import *
from datetime import datetime
import winsound
import pandas as pd


async def sprint12(lithic):
    # Connect to the sensor system
    print('Checking server connection')
    res = await lithic.connectToServer()

    # Quit program if connection is not established
    if res == "Server unreachable":
        print(res)
        raise SystemExit
    else:
        print('OK')
    str(input('Press ENTER to start the test: '))
    print('OK')

    while True:
        # Output a message informing the user of the appropriate sensor module orientation.
        print("\n******** INTRODUCTION TO THE TEST. PLEASE READ CAREFULLY ********\n")
        print("You will be performing Respiratory Rate vs. Music Type and Cardiac Rate vs. Music Type consecutively.\n")
        print(
            "After you are done listening to an audio, you will hold your breath for 15s for the Lithic sensor to detect your heart rate.\n")
        input("\nPress any key to confirm that you have read the introduction: ")
        print("\n\n******** IMPORTANT INSTRUCTION. PLEASE READ CAREFULLY ********\n")
        print(
            'Please orient your Lithic sensor so the rounded case of sensor is facing up to your chin, the LED'
            ' light points away from you,')
        print('and the sensor is hold firmly on your chest with your left hand.\n'
              '\nPlease sit up straight and still on your seat, and breathe normally for 1 minute during the audio play time.\n'
              '\nEnsure that the sensor has a direct contact to your chest (no clothing layer) and the flat'
              ' side of the sensor is pressed firmly on your chest.\n')
        print(
            "After you listen to an audio, you will take a deep breath and hold it for 15s for the sensor to record your heart beats."
            "\nDO NOT SPEAK OR MOVE YOUR BODY DURING THIS PERIOD")
        input('\nConfirm that you have read all the instructions by pressing any key: ')
        print("\n******** TEST BEGINS HERE ********\n")

        count_lst_b = []  # A list contains number of breath counts
        count_lst_h = []  # A list contains number of heart beat counts

        while True:
            try:
                # User prompt: User's full name
                user = str(input('\n\nEnter Full Name of Participant: '))
            except ValueError:
                print('ERROR: Name of user must be a string')
            else:
                if user.find(' ') == -1:
                    print('ERROR: Name should include first and last name, separated by a space')
                else:
                    break
        # User prompt: Tester's Full Name
        while True:
            try:
                tester = str(input('\n\nEnter Full Name of Tester: '))
            except ValueError:
                print('ERROR: Name of tester must be a string')
            else:
                if tester.find(' ') == -1:
                    print('ERROR: Name should include first and last name, separated by a space')
                else:
                    break

        # Begin the recording
        # while True:
        count_lst = []
        for i in range(1, 7):
            print('\nRecording begins after BEEP. Put your sensor on your chest and adjust your posture as instructed.')
            sleep(1)
            winsound.Beep(500, 1350)  # Beep sound at 500Hz for 1.35s
            await lithic.startRecording()
            print('\nDon\'t forget to breathe normally :)')

            # Start the audio files and record while they are playing
            if i == 1:  # First audio file: silent. Intended to measure the current normal heart rate
                sleep(60)  # 60s of silence :(
                print("\nNow take a deep breath and hold it for 15s to measure heart rate...")
                print("Remember NOT to speak or move")
                sleep(20)  # Account for 5s of taking breath in to hold breath and for gyro to readjust

            if i == 2:  # Second audio file: metal
                metal()
                print("\nNow take a deep breath and hold it for 15s to measure heart rate...")
                print("Remember NOT to speak or move")
                sleep(20)  # Account for 5s of taking breath in to hold breath and for gyro to readjust

            elif i == 3:  # Third audio file: R&B
                RB()
                print("\nNow take a deep breath and hold it for 15s to measure heart rate...")
                print("Remember NOT to speak or move")
                sleep(20)  # Account for 5s of taking breath in to hold breath and for gyro to readjust

            elif i == 4:  # Fourth audio file: Jazz, and so forth with i = 5 and i = 6
                Jazz()
                print("\nNow take a deep breath and hold it for 15s to measure heart rate...")
                print("Remember NOT to speak or move")
                sleep(20)  # Account for 5s of taking breath in to hold breath and for gyro to readjust

            elif i == 5:  # Classical
                classic()
                print("\nNow take a deep breath and hold it for 15s to measure heart rate...")
                print("Remember NOT to speak or move")
                sleep(20)  # Account for 5s of taking breath in to hold breath and for gyro to readjust

            elif i == 6:  # Rap
                print("This is the last audio")
                Rap()
                print("\nNow take a deep breath and hold it for 15s to measure heart rate...")
                print("Remember NOT to speak or move")
                sleep(20)  # Account for 5s of taking breath in to hold breath and for gyro to readjust

            # End recording and ask for recorded data
            res = await lithic.stopRecording()
            print("Recording stop.")
            file = open('data.txt', 'w')  # Opens file for writing, creates file if it does not exist
            file.write("timestamp,acc_x,acc_y,acc_z,gyro_x,gyro_y,gyro_z\n")
            # Initialize lists
            signal1 = []  # For respiratory signal
            signal2 = []  # For heart signal
            data = json.loads(res)
            file = open('data.txt', 'a')
            for frame in data:
                for sample in frame['samples']:
                    file.write(sample + "\n")
                    sample_list = sample.strip().split(',')
                    signal1.append(float(sample_list[4]))  # Grab the gyro's x-axis for respiration
                    signal2.append(float(sample_list[5]))  # Grab the gyro's y-axis for cardiac
            file.close()

            # Parameters for signal analysis
            fs = 250
            fc = 20
            order = 10
            sos = butter(order, fc, 'low', False, 'sos', fs)

            # **** ANALYZE SIGNAL 1: RESPIRATORY RATE ****
            signal1_filtered = sosfilt(sos, signal1).tolist()
            # 1 peak above threshold = 1 breath
            threshold = 2  # Based on my observation
            breath_lst = []  # A list that contains all data points above threshold
            # Grab index of data points. We need to group data that classifies as 1 breath.
            for breath in signal1_filtered:
                if breath >= threshold:
                    breath_lst.append(breath)

            # Based on my observation of my own respiratory rate, each inhalation has around 350 data points.
            # I will equate 350 data points as 1 breath. This value can vary depends on the person.
            chunk = 350  # A chunk of 350 data points
            breath_cycle = [breath_lst[n:n + chunk] for n in range(0, len(breath_lst), chunk)]
            count_breath = len(breath_cycle)
            count_lst_b.append(count_breath)

            # **** ANALYZE SIGNAL 2: HEART RATE ****
            signal2_filtered = sosfilt(sos, signal2).tolist()
            # 1 peak above threshold = 1 heart beat
            threshold = -1.8  # Based on my observation
            beat_lst = []  # A list that contains all data points above threshold
            # Grab index of data points. We need to group data that classifies as 1 beat.
            for beat in signal2_filtered:
                if beat >= threshold:
                    beat_lst.append(beat)

            # Based on my observation of my own cardiac rate, each beat has around 200 data points.
            # I will equate 200 data points as 1 beat. This value can vary depends on the person.
            chunk = 200  # A chunk of 200 data points

            # Remove the first 75 data points, because those data points are recording when I take a deep
            # breath and the gyro's y-axis has not been stabilized. This is based on observation.
            # Then remove the gyro's y-axis data during the respiratory recording as well = around 13k data points
            # Total: 13327 data points removed before appending the list of accepted BPM value
            beat_cycle = [beat_lst[n:n + chunk] for n in range(13327, len(beat_lst), chunk)]
            count_beat = len(beat_cycle)
            BPM = count_beat * 4  # To estimate number of heart beats per min (BPM), multiply by 4 after recording the number of beats for 15s (clinical application)
            count_lst_h.append(BPM)
            print("\nBreak for 30s to regulate your breathing again. You can relax for now.")
            sleep(30)  # Between each audio file, I put a 30s rest so the user can regulate their breathing

        # The result will be appended to a table result in a file
        date = datetime.now().strftime('%d %b %Y')  # Get the current date
        t = datetime.now().strftime('%I:%M:%S %p')  # Get the current time
        music_type = ["No audio", "Metal", "R&B", "Jazz", "Classical", "Rap"]
        data = {'Number of Breaths': count_lst_b, 'Estimated BPM': count_lst_h}
        print("\nHi, " + user + "! Here is your result")
        print("\nTest Date: " + str(date))
        print("\nTest Time: " + str(t))
        print("\nName of Tester: " + tester)
        print("\n***** Your Respiratory & Cardiac Rate vs. Music Type Results *****")
        df = pd.DataFrame(data=data, index=music_type,
                          columns=["Number of Breaths", "Estimated BPM"])  # Turn list into pandas' data frame for table
        print(df)
        print("\nYour result table is saved in a separate file")
        print(
            "\nDISCLAIMER: if your BPM returns too small or 0, it means the sensor is unable to pick up your heart rate")
        print("In this case, it is advisable that you perform the test again.")

        # **** TABLE OF RESULT SPECIFICATION FOR SPRINT 12 AND SAVE IT INTO A FILE ****
        filename = '{}_{}_Test.txt'.format(user.replace(' ', '_'), date.replace(' ', '_'))  # File naming convention
        file = open(filename, 'w')
        file.write("*** RESPIRATORY RATE & CARDIAC RATE VS. MUSIC TYPE TEST ***")
        file.write("\nTest Date: " + str(date))
        file.write("\nTest Time: " + str(t))
        file.write("\nName of Participant: " + user)
        file.write("\nName of Tester: " + tester + "\n\n")
        file.write(df.to_string())  # Turn pandas df into string aka. a way to store a txt file
        file.write(
            "\nDISCLAIMER: if your BPM returns too small or 0, it means the sensor is unable to pick up your heart rate")
        file.write("In this case, it is advisable that you perform the test again.")
        file.close()

        # Ask for user input when the program is done
        user_input = str(input(
            "Would you like to perform another test?\nPress 'n' or 'q' to quit. Press other keys to continue: "))

        if user_input == 'q' or user_input == 'n':
            print("Thank you for taking the test. Goodbye :)")
            quit()
        else:
            continue


async def main():
    await sprint12(Lithic())


asyncio.run(main())
